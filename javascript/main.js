$(document).ready(function () {

    var checkBrowser = function () {
        if (!Modernizr.flexbox && (navigator.appVersion.indexOf("MSIE 10") == -1)) {
            if (!window.XMLHttpRequest && 'ActiveXObject' in window) {
                window.XMLHttpRequest = function () {
                    return new ActiveXObject('MSXML2.XMLHttp');
                };
            }
            var xhr = new XMLHttpRequest();
            xhr.open('GET', '/outdated-browser.php', true);
            xhr.onreadystatechange = function () {
                if (this.readyState !== 4) return;
                if (this.status !== 200) return; // or whatever error handling you want
                document.getElementById('page').innerHTML = this.responseText;
            };
            xhr.send();
        }
    };

    checkBrowser();

    $('body').foundation();

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        navText: ['<i class="icon icon-prev"></i>', '<i class="icon icon-next"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: true
            },
            900: {
                items: 5,
                nav: true
            },
            1200: {
                items: 6,
                nav: true
            }
        }
    });
    // Placeholder
    $('input, textarea').placeholder();

    // Phone mask
    $('.phone').mask("+7(999) 999-9999");

    // Инициализируем плагин валидации
    try {
        // Добавим плагину новый метод валидации, который позволит проводить валидацию поля по регулярному выражению
        $.validator.addMethod("regx", function (value, element, regexpr) {
            return regexpr.test(value);
        }, "Неверный номер телефона.");

        var validationBehavior = {
            rules: {
                name: {
                    required: true
                },
                surname: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    regx: /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/
                },
            },
            messages: {
                name: "Поле \"Имя\" не заполнено",
                surname: "Поле \"Фамилия\" не заполнено",
                email: {
                    required: "Укажите почтовый адрес",
                    email: "Неверный почтовый адрес"
                },
                phone: {
                    required: "Укажите номер телефона",
                    regx: 'Неверный номер телефона'
                },
            },
            highlight: function (element, errorClass) {
                $(element).addClass('form__field--state-error');
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass('form__field--state-error');
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            errorElement: "small",
            errorClass: "form__field-error-mess"
        };

        $('form').each(function (item, element) {
            $(element).validate(validationBehavior);
        });

    } catch (exception) {
        console.log(exception.message);
    }

    $('#header__action').on('click', function (e) {
        e.preventDefault();
        $('#header__action, #header__nav').toggleClass('open')
    });
});